﻿Shader "Custom/CurvingMatCap"
{
	Properties
	{
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_MatCap ("MatCap (RGB)", 2D) = "white" {}
		_Curvature ("Curvature magnitude", Range (0.01, 150.0)) = 0.01
		_XCurvature (" X Curvature magnitude", Range (0.01, 150.0)) = 0.01
		_Offset ("Offset", Range (-10, 10)) = 0.01

	}
	
	Subshader
	{
		Tags { "RenderType"="Opaque" }
		
		Pass
		{

			Tags { "LightMode" = "Always" }
			
			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_fastest
				#include "UnityCG.cginc"
				
				struct v2f
				{
					float4 pos	: SV_POSITION;
					float2 uv 	: TEXCOORD0;
					float2 cap	: TEXCOORD1;
				};
				
				uniform float4 _MainTex_ST;
				float _Curvature;
				float _XCurvature;
				float _Offset;

						half4 Bend(half4 v)
			{
				half4 wpos = mul(unity_ObjectToWorld, v);

				float zDist = wpos.z /  _Curvature;
				float dist = length(zDist);
				wpos.y -= dist * dist ;

				float xDist = (wpos.x + _Offset)/  _XCurvature;
				dist = length(xDist);
				wpos.y -= dist * dist ;

				wpos = mul(unity_WorldToObject, wpos);
				wpos = mul(UNITY_MATRIX_MVP, wpos);
				return wpos;
			}

				v2f vert (appdata_base v)
				{
					v2f o;
					o.pos = Bend(v.vertex);
					//o.pos = mul (UNITY_MATRIX_MVP, v.vertex);
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
					half2 capCoord;
					
					float3 worldNorm = normalize(unity_WorldToObject[0].xyz * v.normal.x + unity_WorldToObject[1].xyz * v.normal.y + unity_WorldToObject[2].xyz * v.normal.z);
					worldNorm = mul((float3x3)UNITY_MATRIX_V, worldNorm);
					o.cap.xy = worldNorm.xy * 0.5 + 0.5;
					
					return o;
				}
				
				uniform sampler2D _MainTex;
				uniform sampler2D _MatCap;
		
		

				fixed4 frag (v2f i) : COLOR
				{
					fixed4 tex = tex2D(_MainTex, i.uv);
					fixed4 mc = tex2D(_MatCap, i.cap);
					
					return tex * mc * 2.0;
				}
			ENDCG
		}
	}
	
	Fallback "VertexLit"
}
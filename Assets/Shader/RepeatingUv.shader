﻿// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'
// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

Shader "Custom/RepeatingUv"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Speed ("Speed", Range (0.0, 1000.0)) = 1.0
		_Curvature ("Curvature magnitude", Range (0.01, 150.0)) = 0.01
		_XCurvature (" X Curvature magnitude", Range (0.01, 150.0)) = 0.01
		_Offset ("Offset", Range (-10, 10)) = 0.01
		_YOffset ("Y Offset", Range (-10, 10)) = 0.01

	}
	SubShader
	{
	Cull Back
	Tags {"Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent"}
	    Blend SrcAlpha OneMinusSrcAlpha
		ZWrite Off
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Speed;
			float _Curvature;
			float _XCurvature;
			float _Offset;
			float _YOffset;

			half4 Bend(half4 v)
			{
				half4 wpos = mul(unity_ObjectToWorld, v);

				float zDist = wpos.z /  _Curvature;
				float dist = length(zDist);
				wpos.y -= dist * dist ;

				float xDist = (wpos.x + _Offset)/  _XCurvature;
				dist = length(xDist);
				wpos.y -= dist * dist ;

				wpos.y -= _YOffset;

				wpos = mul(unity_WorldToObject, wpos);
				wpos = mul(UNITY_MATRIX_MVP, wpos);
				return wpos;
			}

			v2f vert (appdata v)
			{
				v2f o;
				
				o.vertex = Bend(v.vertex);
				//o.vertex =mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.uv, _MainTex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
		
				float y = (i.uv.y - _Time * _Speed);
		//		y  -= floor(y);
		//		y =  y * 30.0 % 1;
				i.uv.y = y;
				fixed4 col = tex2D(_MainTex, i.uv);
	
				return col;
			}
			ENDCG
		}
	}
}

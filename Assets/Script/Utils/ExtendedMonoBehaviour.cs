﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class ExtendedMonoBehaviour : MonoBehaviour 
{
    const int kMaxRecusriveCalls = 10;

    public T SafeGetComponent<T>()
    {
        T component = GetComponent<T>();

#if DEBUG
        if (component == null)
        {
            Debug.LogError(gameObject + ": Expected to find component of type "
               + typeof(T) + " but found none", gameObject);
        }
#endif

        return component;
    }

    public GameObject SafeFindChild(string name)
    {
        Transform child = transform.FindChild(name);
        if (child == null)
        {
#if DEBUG
            Debug.LogError(gameObject + ": Expected to find child named "
               + name + " but found none.");
#endif

            return null;
        }

        return child.gameObject;
    }

    protected T GetComponentFromCollider<T>(Collider2D source)
    {
        T result = default(T);
        Rigidbody2D rigidBody = source.attachedRigidbody;
        if (rigidBody)
            result = rigidBody.GetComponent<T>();

        if (System.Object.Equals(default(T), result))
            result = source.GetComponent<T>();

        return result;
    }

    // Breadth first recusrive search for a child named "targetName"
    protected Transform RecursiveChildSearch(string targetName, Transform curTransform, float deepness = 0)
    {
        if (deepness > kMaxRecusriveCalls)
            return null;

        foreach(Transform child in curTransform)
        {
            if (child.name == targetName)
                return child;
        }

        foreach (Transform child in curTransform)
        {
            Transform childResult = RecursiveChildSearch(targetName, child, deepness + 1);

            if (childResult != null)
                return childResult;
        }

        return null;
    }

    protected void RecursiveSearchOfComponentsInChildren(Transform root, 
        List<List<Component>> outputLists, List<Type> types)
    {
        if (outputLists.Count != types.Count)
        {
            Debug.LogError("Mismatched number of output lists and types");
            return;
        }

        RecursiveSearchOfComponentsInChildren(root, outputLists, types, 0);
    }

    void RecursiveSearchOfComponentsInChildren(Transform root,
        List<List<Component>> outputLists, List<Type> types, int level)
    {
        if (level > 6)
            return;


        foreach (Transform child in root)
        {
            int i = 0;
            foreach(Type curType in types)
            {
                Component component = child.GetComponent(curType);
                if (component != null)
                    outputLists[i].Add(component);

                ++i;
            }

            RecursiveSearchOfComponentsInChildren(child, outputLists, types, level + 1);
        }
    }
}

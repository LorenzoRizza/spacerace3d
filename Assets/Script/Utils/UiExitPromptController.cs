﻿using UnityEngine;
using System.Collections;

public class UiExitPromptController : MonoBehaviour {

    void OnEnable()
    {
        if (GameManager.Instance.IsPlaying())
        {
            GameManager.Instance.PauseTheGame();
        }
    }
}

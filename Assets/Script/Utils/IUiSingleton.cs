﻿using UnityEngine;
using System.Collections;

public interface IUiSingleton 
{
    IUiSingleton GetStaticInstance();
    void SetEnabled(bool enabled);
    void OnPoppedFromUiScreenStack();
    bool CanBackOut();
    bool IsOverlay();
}

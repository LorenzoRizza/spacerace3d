﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class UiManager : MonoSingleton<UiManager> 
{
    public bool isDebugEnabled;
    private Stack<IUiSingleton> screensStack = new Stack<IUiSingleton>();
    const string kDefaultSprite = "Textures/DefaultTexture";

    protected override void Awake()
    {
        base.Awake();
        PushUiScreen(UiTapToStart.Instance);
#if DEMO
        demoNotifier.SetActive(true);
#endif
    }

 
    bool ApproximateRatio(float ratio1, float ratio2)
    {
        float difference = ratio1 / ratio2;
        return 0.99f < difference && difference < 1.01f;
    }

    public T InstantiateUi<T>() where T: UiSingleton<T>
    {
        string moduleName = "Prefab/Ui/Modules/" + typeof(T).Name;
        GameObject newModule = (GameObject)GameObject.Instantiate(Resources.Load(moduleName));

        if (newModule)
        {
            newModule.transform.SetParent(transform, false);
            return newModule.GetComponent<T>();
        }
     
        Debug.LogWarning("UiManager: Tried to instantiate a UI module, but it is not in the UiManagerList: " + typeof(T).ToString());
        return default(T);
    }

    void Update()
    {
        //if (Input.GetKey(KeyCode.Escape))
        //{
        //    UiManager.instance.PushUiScreen(UiPreExitPromt.Instance);
        //    return;
        //}
    }

    public void PushUiScreen(IUiSingleton to) 
    {
        bool enable = true;
        bool disablePrevious = ! to.IsOverlay();
        bool push = true;
        if (screensStack.Count == 0)
        {
            disablePrevious = false;
        }
        else if (to == screensStack.Peek())
        {
            enable = true;
            disablePrevious = false;
            push = false;
        }
        else if (screensStack.Contains(to))
        {
            PopToScreen(to);
            return;
        }
        else
        {
            disablePrevious = disablePrevious || screensStack.Peek().IsOverlay();
        }

        if (disablePrevious)
        {
            screensStack.Peek().SetEnabled(false);
        }

        if (push)
        {
            screensStack.Push(to);
        }

        if (enable)
        {
            to.SetEnabled(false);
            to.SetEnabled(true);
        }
    }

    public void PopUiScreen(bool force = false)
    {
        if (screensStack.Count == 0 || (! force && ! screensStack.Peek().CanBackOut()))
        {
            return;
        }

        SimpleUiScreenPop();

        if (screensStack.Count > 0)
            screensStack.Peek().SetEnabled(true);
    }

    void SimpleUiScreenPop()
    {
        screensStack.Peek().SetEnabled(false);
        screensStack.Peek().OnPoppedFromUiScreenStack();
        screensStack.Pop();
    }


    public void PopToScreen(IUiSingleton to)
    {
        if ( ! screensStack.Contains(to))
        {
            Debug.LogWarning("UiManager: Tried to pop back to screen " + to.ToString() + " but it is not in the UI stack.");
            return;
        }

        while(to != screensStack.Peek() && screensStack.Count > 0)
        {
            SimpleUiScreenPop();
        }

        if (screensStack.Count > 0)
            screensStack.Peek().SetEnabled(true);
    }

    public bool DoesStackContains(IUiSingleton screen)
    {
        return screensStack.Contains(screen);
    }

    public bool IsStackTop(IUiSingleton screen)
    {
        return screensStack.Peek() == screen;
    }
}

﻿using UnityEngine;
using System.Collections;

public class UiTapToStart : UiSingleton<UiTapToStart>
{
    public Animator myAnimator = null;

    int hash = Animator.StringToHash("Exit");

    enum EButtonPressed
    {
        Play,
        Info,
        Shop
    }

    EButtonPressed activatedButton;

    public void Exit()
    {
        Application.Quit();
    }

    public void Shop()
    {
        activatedButton = EButtonPressed.Shop;
        myAnimator.SetBool(hash, true);
    }

    public void Play()
    {
        activatedButton = EButtonPressed.Play;
        myAnimator.SetBool(hash, true);
    }

    public void Info()
    {
        activatedButton = EButtonPressed.Info;
        myAnimator.SetBool(hash, true);
    }

    public void OnExitAnimationCompleted()
    {
        switch (activatedButton)
        {
            case EButtonPressed.Play:
                {
                    UiManager.Instance.PopUiScreen();
                    GameManager.Instance.StartGame();
                    break;
                }
            case EButtonPressed.Info:
                {
                    UiManager.Instance.PopUiScreen();
                    UiManager.Instance.PushUiScreen(UiInfo.Instance);
                    break;
                }
            case EButtonPressed.Shop:
                {
                    UiManager.Instance.PushUiScreen(UiShop.Instance);
                    break;
                }
        }
    }

    public override void OnSettingEnabled(bool value)
    {
        if (value)
            myAnimator.SetBool(hash, !value);
        UiPauser.Instance.Diplay(!value);
    }
}

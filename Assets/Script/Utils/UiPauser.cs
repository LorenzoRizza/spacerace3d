﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiPauser : MonoSingleton<UiPauser> {

    public Sprite pause = null;
    public Sprite unPause = null;
    public Image buttonImage = null;
    public Text pauseText = null;
    public GameObject exitButton = null;

   public void Diplay(bool value)
    {
        transform.GetChild(0).gameObject.SetActive(value);
        exitButton.SetActive(false);
    }
    public void Exit()
   {
       Application.Quit();
   }
    public void Pause()
    {
        if(GameManager.Instance.IsPlaying())
        {
            GameManager.Instance.PauseTheGame();
            buttonImage.sprite = unPause;
            pauseText.text = "UNPAUSE";
            exitButton.SetActive(true);
        }
        else
        {
            GameManager.Instance.UnpauseTheGame();
            buttonImage.sprite = pause;
            pauseText.text = "PAUSE";
            exitButton.SetActive(false);
        }
    }
}

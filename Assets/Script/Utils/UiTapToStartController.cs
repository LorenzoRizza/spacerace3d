﻿using UnityEngine;
using System.Collections;

public class UiTapToStartController : MonoBehaviour {

    void Update()
    {
        if (Input.touches.Length > 0 || Input.GetButtonDown("Left"))
        {
            UiManager.Instance.PushUiScreen(UiSelectTrack.Instance);
        }
    }
}

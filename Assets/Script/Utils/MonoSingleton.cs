﻿using UnityEngine;
using System.Collections;

public class MonoSingleton<T> : ExtendedMonoBehaviour where T : MonoSingleton<T>
{
    public static T Instance
    {
        get
        {
            if (!HasInstance())
            {
                CreateInstance();
            }
            return instance;
        }
    }

    protected static T instance = null;
    
    static Transform scriptsParent = null;

    protected virtual void Awake()
    {
        if (instance != null)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
            instance = (T)this;
    }

    public static bool HasInstance()
    {
        return instance != null;
    }

    static void CreateInstance()
    {
        GameObject newObject = new GameObject(typeof(T).Name);
        T newSingleton = newObject.AddComponent<T>();

        if (newSingleton.DestroyOnLoad())
        {
            //if (scriptsParent == null)
            //    scriptsParent = GameManagerStarter.Instance.transform.parent;

            newObject.transform.SetParent(scriptsParent);
        }
        else
            DontDestroyOnLoad(newObject);
    }

    protected virtual bool DestroyOnLoad() { return true; }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public abstract class  UiSingleton<T> : MonoBehaviour, IUiSingleton where T : UiSingleton<T>
{
    public bool canBackOut = false;
    public Selectable defaultButton = null;

    protected bool IsEnabled { get; private set; }

    public static T Instance
    {
        get
        {
            if (instance == null)
                UiManager.Instance.InstantiateUi<T>();
            return instance;
        }
    }
    protected static T instance;

    protected virtual void Awake()
    {
        if (instance != null)
        {
            Debug.LogError("UiSingleton is instanciated twice.");
            Destroy(this);
        }
        instance = (T)this;
        IsEnabled = false;
    }

    public static bool HasInstance()
    {
        return instance != null;
    }

    public virtual void SetEnabled(bool enabled)
    {
        for (int i = 0; i < transform.childCount; ++i)
        {
            transform.GetChild(i).gameObject.SetActive(enabled);
        }

        if (enabled && defaultButton)
        {
            defaultButton.Select();
        }

        IsEnabled = enabled;
        OnSettingEnabled(enabled);
    }

    public virtual void OnSettingEnabled(bool value)
    {

    }

    public IUiSingleton GetStaticInstance()
    {
        return Instance;
    }

    public bool CanBackOut()
    {
        return canBackOut;
    }

    public virtual void OnPoppedFromUiScreenStack()
    {

    }

    public virtual bool IsOverlay()
    {
        return false;
    }

    public static bool IsOnTop()
    {
        return HasInstance() && UiManager.HasInstance() && UiManager.Instance.IsStackTop(instance);
    }

    public static bool IsInStack()
    {
        return HasInstance() && UiManager.HasInstance() && UiManager.Instance.DoesStackContains(instance);
    }
}

﻿using UnityEngine;
using System.Collections;

public class GameManager : MonoSingleton<GameManager> {


    public delegate void OnUnpaused();
    public event OnUnpaused onUnpaused;

    public delegate void OnPaused();
    public event OnPaused onPaused;

    public delegate void OnRestarting();
    public event OnRestarting onRestart;

    enum  Estate
    {
        Paused,
        Playing
    }

    Estate state;

    protected override void Awake()
    {
        base.Awake();
        SetState(Estate.Paused, true);
    }


    public void StartGame()
    {
        onRestart.Invoke();
        if (state == Estate.Paused)
            SetState(Estate.Playing);
    }

    public bool IsPlaying()
    {
        return state == Estate.Playing;
    }

    public void PauseTheGame()
    {
        SetState(Estate.Paused);
    }

    public void UnpauseTheGame()
    {
        SetState(Estate.Playing);
    }

    void SetState(Estate newState, bool init = false)
    {
        if(state != newState || init)
        {
            state = newState;
            Time.timeScale = state == Estate.Paused ? 0f : 1f;
            if (onUnpaused!= null && state == Estate.Playing) onUnpaused.Invoke();
            else if( onPaused != null) onPaused.Invoke();
        }
    }
}

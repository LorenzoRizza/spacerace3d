﻿using UnityEngine;
using System.Collections;

public class UiShop : UiSingleton<UiShop>{

    public Animator myAnimator = null;

    int hash = Animator.StringToHash("Exit");

    enum EButtonPressed
    {
        Back,
        Audio,
        Models,
        Planets
    }

    EButtonPressed activatedButton;

    public void OnAudioTab()
    {
        activatedButton = EButtonPressed.Audio;
        myAnimator.SetBool(hash, true);
    }

    public void OnBack()
    {
        activatedButton = EButtonPressed.Back;
        myAnimator.SetBool(hash, true);
    }

    public void OnExitAnimationCompleted()
    {
        switch (activatedButton)
        {
            case EButtonPressed.Audio:
                {
                    UiManager.Instance.PushUiScreen(UiSelectTrack.Instance);
                    break;
                }
            case EButtonPressed.Back:
                {
                    UiManager.Instance.PushUiScreen(UiTapToStart.Instance);
                    break;
                }
        }
    }

    public override void OnSettingEnabled(bool value)
    {
        if (value)
            myAnimator.SetBool(hash, !value);
    }

}

﻿using UnityEngine;
using System.Collections;

public class UiPreExitPromt : UiSingleton<UiPreExitPromt> {

    public Animator myAnimator = null;

    int hash = Animator.StringToHash("Exit");
    public bool animationCompleted = false;
    bool IsEnabled = false;

    public void OnPressingAnyware()
    {
        myAnimator.SetBool(hash, true);
    }

    public void OnenteringAnimationCompleted()
    {
        animationCompleted = true;
    }

    public void OnExitAnimationCompleted()
    {
        UiManager.Instance.PopUiScreen();
        GameManager.Instance.UnpauseTheGame();
        animationCompleted = false;
    }

    void Update()
    {
        if (IsEnabled && animationCompleted && Input.GetKey(KeyCode.Escape)) 
        {
            Application.Quit();
        }
    }

    public override void OnSettingEnabled(bool value)
    {
        IsEnabled = value;
        if (value)
            myAnimator.SetBool(hash, !value);
    }
}

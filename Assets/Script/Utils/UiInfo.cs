﻿using UnityEngine;
using System.Collections;

public class UiInfo : UiSingleton<UiInfo>
{
    public Animator myAnimator = null;

    int hash = Animator.StringToHash("Exit");

    public void Play()
    {
        myAnimator.SetBool(hash, true);
    }

    public void OnExitAnimationCompleted()
    {
        UiManager.Instance.PopUiScreen();
        GameManager.Instance.StartGame();
    }

    public override void OnSettingEnabled(bool value)
    {
        if (value)
            myAnimator.SetBool(hash, !value);
    }
}

﻿using UnityEngine;
using System.Collections;

public class UiSelectTrack : UiSingleton<UiSelectTrack> {

    public Animator myAnimator = null;

    int hash = Animator.StringToHash("Exit");

    enum EButtonPressed
    {
        Play,
        Info,
        Back
    }

    EButtonPressed activatedButton;

    public void SongZero()
    {
        MusicManager.Instance.SetTrack(0);
    }

    public void SongOne()
    {
        MusicManager.Instance.SetTrack(1);
    }

    public void OnBack()
    {
        activatedButton = EButtonPressed.Back;
        myAnimator.SetBool(hash, true);
    }

    public void OnExitAnimationCompleted()
    {
        switch (activatedButton)
        {
            case EButtonPressed.Back:
                {
                    UiManager.Instance.PushUiScreen(UiTapToStart.Instance);
                    break;
                }
        }
    }

    public override void OnSettingEnabled(bool value)
    {
        if (value)
            myAnimator.SetBool(hash, !value);
    }
}

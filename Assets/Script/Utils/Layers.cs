﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Layers : MonoSingleton<Layers>
{
    static public LayerMask kGround = LayerMask.NameToLayer("Ground");
    static public LayerMask kPlayer = LayerMask.NameToLayer("Player");
    static public LayerMask kObstacles = LayerMask.NameToLayer("Obstacles");
    static public LayerMask kActivator = LayerMask.NameToLayer("Activator");
  
#if DEBUG
    protected override void Awake()
    {
        base.Awake();
        List<LayerMask> layerMasks = new List<LayerMask>();
        layerMasks.Add(kGround);
        layerMasks.Add(kPlayer);
        layerMasks.Add(kObstacles);
        layerMasks.Add(kActivator);

        foreach (LayerMask layerMask in layerMasks)
        {
            if (layerMask == -1)
            {
                Debug.LogWarning("Layers: Awake: cannot find one of the layers.");
            }
        }
    }
#endif // debug

    public static LayerMask ToByteMask(LayerMask original)
    {
        return 1 << original;
    }

    public static LayerMask ToByteMaskMulti(params LayerMask [] masks)
    {
        LayerMask result = 0;
        for (int i = 0; i < masks.Length; ++i)
            result = result | 1 << masks[i];
        return result;
    }

    public static LayerMask AddByteToMask(LayerMask a, int b)
    {
        return a | 1 << b;
    }
}

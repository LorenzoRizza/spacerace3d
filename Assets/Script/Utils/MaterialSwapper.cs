﻿using UnityEngine;
using System.Collections;

public class MaterialSwapper : ExtendedMonoBehaviour {

    public Material newMaterialTemplate = null;
    Renderer myRenderer = null;
    Material newMaterialInstance = null;
    Material originalMaterial = null;
    bool swapped = false;

    void Awake()
    {
        myRenderer = SafeGetComponent<Renderer>();
        originalMaterial = myRenderer.material;
    }

    public void SwapMaterial()
    {
        if (!swapped)
        {
            if (newMaterialInstance != null)
            {
                myRenderer.material = newMaterialInstance;
            }
            else
            {
                myRenderer.material = newMaterialTemplate;
                newMaterialInstance = myRenderer.material;
            }
        }
        else
            myRenderer.material = originalMaterial;
        swapped = !swapped;
    }

    public void Reset()
    {
        if(swapped)
        {
            SwapMaterial();
        }
    }
}

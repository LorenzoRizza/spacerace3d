﻿using UnityEngine;
using System.Collections;

public class Player : MonoSingleton<Player>
{
    public bool IsDying { get { return timer > 0; } }
    public SkinnedMeshRenderer mesh = null;
    public float timeToDisasseble = 2f;
    float timer = 0f;
    
    void Start()
    {
        GameManager.instance.onRestart += Reset;
    }

    private void Reset()
    {
        Time.timeScale = 1f;
        mesh.SetBlendShapeWeight(0, 0);
    }

    public void ExecuteEndAnimation()
    {
        timer = timeToDisasseble;
    }

    void Update()
    {
        if (timer > 0)
        {
            timer -= Time.unscaledDeltaTime;

            if (timer <= 0)
            {
                GameManager.Instance.StartGame();
                GameManager.Instance.PauseTheGame();
                UiManager.Instance.PushUiScreen(UiTapToStart.Instance);
            }
            else
            {
                float weight = Mathf.Lerp(0f, 100f, (timeToDisasseble - timer) / timeToDisasseble);
                Time.timeScale = 1f - weight / 100f;
                mesh.SetBlendShapeWeight(0, weight);
            }
        }
    }
}

﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
    [Range(0f,1f)]
    public float cameraMovementStep = 0.05f;
    Transform playerPos = null;
    Transform PlayerPosition { get { if (!playerPos) playerPos = Player.Instance.transform; return playerPos; } }
    Transform myTransform = null;
    Transform MyTransform { get { if (!myTransform) myTransform = transform; return myTransform; } }


    void Awake()
    {
        GameManager.Instance.onRestart += Reset;
    }

    void Reset()
    {
        MyTransform.position = new Vector3(4f, MyTransform.position.y, MyTransform.position.z);
    }

    void Update()
    {
        if (!GameManager.Instance.IsPlaying())
            return;
        float newX = Mathf.Lerp(MyTransform.position.x, PlayerPosition.position.x, cameraMovementStep);
        MyTransform.position = new Vector3(newX, MyTransform.position.y, MyTransform.position.z);
    }
}

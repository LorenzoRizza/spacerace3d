﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    Renderer myRenderer = null;

    void Start()
    {
        myRenderer = GetComponent<Renderer>();
    }

    public void Activate()
    {
        myRenderer.enabled = true;
    }

    public void Deactivate()
    {
        myRenderer.enabled = false;
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScoreModule : MonoSingleton<ScoreModule> {

    public Text text = null;
    int score = 500;

    protected override void Awake()
    {
        base.Awake();
        GameManager.Instance.onRestart += Reset;
    }

    private void Reset()
    {
        score = 500;
    }

    void Update()
    {
        if (Player.Instance.IsDying) return;

        if (!GameManager.Instance.IsPlaying())
        {
            if (text.isActiveAndEnabled) text.enabled = false;
            return;
        }
        else
        {
            if (!text.isActiveAndEnabled) text.enabled = true;
        }

        if (LaneManager.Instance.IsPlayerOnTriggeredLane())
        {
            ++score;
        }

        score = Mathf.Max(0, score);

        if (score == 0)
        {
            GameManager.Instance.StartGame();
            GameManager.Instance.PauseTheGame();
            UiManager.Instance.PushUiScreen(UiTapToStart.Instance);
        }

        text.text = score.ToString();
    }

    public void GetDamage()
    {
        if(score > 50)
        {
            score /= 2;
        }
        else
        {
            score = 0;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        //score -= 1000;
    }
}

﻿using UnityEngine;
using System.Collections;

public class PlayerMover : MonoBehaviour {

    public float border = 4f;

    int currentLane { get { return LaneManager.Instance.CurrentLane;} set{ LaneManager.Instance.CurrentLane = value;} }
    int targetLane { get { return LaneManager.Instance.TargetLane; } set { LaneManager.Instance.TargetLane = value; } }


    float timer = 0f;
    bool wantToGoLeft = false;
    bool wanttoGoRight = false;
    Rigidbody rb = null;
    private bool reverseLeft = false;
    private bool reverseRight = false;
    private bool canReceiveNewInput = true;
    

    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        GameManager.Instance.onRestart += Reset;
    }

    void Reset()
    {
        timer = 0f;
        wantToGoLeft = false;
        wanttoGoRight = false;
        reverseLeft = false;
        reverseRight = false;
        Vector3 newPos = new Vector3(4f, 0f, 0f);
        transform.position = newPos;
    }

    void Start()
    {
        Vector3 newPos = new Vector3(GetLaneX(currentLane), 0f, 0f);
        rb.MovePosition(newPos);
    }

	void Update () {
        if (Player.Instance.IsDying) return;

        ManageInput();
        SetTargetLane();
	}

    void FixedUpdate()
    {
        UpdatePosition();
    }

    private void ManageInput()
    {
//#if UNITY_EDITOR
         if(currentLane == targetLane)
         {
             wantToGoLeft = Input.GetButtonDown("Left") && currentLane != 0;
             if (!wantToGoLeft)
             {
                 wanttoGoRight = Input.GetButtonDown("Right")  && currentLane != LaneManager.Instance.numberOfLanes - 1;
             }
         }
//#else
//        if(Input.touchCount == 0) canReceiveNewInput = true;

//        if(currentLane == targetLane && Input.touchCount >0 && canReceiveNewInput)
//        {
//           Touch touch =  Input.touches[0];

//           float halfScreenSizeInPixel = Screen.width / 2f;

//           wantToGoLeft = touch.position.x < halfScreenSizeInPixel && currentLane != 0;
//            if(!wantToGoLeft)
//            {
//                wanttoGoRight = touch.position.x > halfScreenSizeInPixel && currentLane != LaneManager.Instance.numberOfLanes - 1;
//            }

//            canReceiveNewInput = false;
//        }
//#endif
    }

    float GetLaneX(int n)
    {
       return LaneManager.Instance.GetLaneX(n);
    }

    private void UpdatePosition()
    {
        if(currentLane != targetLane)
        {
            timer += Time.deltaTime;
            float newX ;
            if(timer >= LaneManager.Instance.timeToSwitchLane)
            {
                timer = 0f;
                currentLane = targetLane;
                newX = GetLaneX(targetLane);
                reverseLeft = false;
                reverseRight = false;
            }
            else 
            {
                if (reverseLeft)
                {
                    if (timer < LaneManager.Instance.timeToSwitchLane / 2f)
                        newX = Mathf.Lerp(0f, -2f * border, timer / LaneManager.Instance.timeToSwitchLane);
                    else
                    {
                        newX = Mathf.Lerp(GetLaneX(LaneManager.Instance.numberOfLanes - 1) + 2f * border, GetLaneX(LaneManager.Instance.numberOfLanes - 1), timer / LaneManager.Instance.timeToSwitchLane);
                    }
                }
                else if( reverseRight)
                {
                    if (timer < LaneManager.Instance.timeToSwitchLane / 2f)
                        newX = Mathf.Lerp(GetLaneX(LaneManager.Instance.numberOfLanes - 1), +2f * border, timer / LaneManager.Instance.timeToSwitchLane);
                    else
                    {
                        newX = Mathf.Lerp(- 2f * border, 0f, timer / LaneManager.Instance.timeToSwitchLane);
                    }
                }
                else
                {
                    newX = Mathf.Lerp(GetLaneX(currentLane), GetLaneX(targetLane), timer / LaneManager.Instance.timeToSwitchLane);
                }
            }
            Vector3 newPos = new Vector3(newX, 0f, 0f);
            rb.MovePosition(newPos);
        }
    }

    

    private void SetTargetLane()
    {
        if(wantToGoLeft)
        {
            --targetLane;
            if(targetLane <0)
            {
                targetLane = LaneManager.Instance.numberOfLanes - 1;
                reverseLeft = true;
            }
            wantToGoLeft = false;
        }
        else if (wanttoGoRight)
        {
            ++targetLane;
            if (targetLane == LaneManager.Instance.numberOfLanes)
            {
                targetLane = 0;
                reverseRight = true;
            }
            wanttoGoRight = false;
        }
    }
}

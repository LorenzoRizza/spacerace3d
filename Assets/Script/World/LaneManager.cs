﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class LaneManager : MonoSingleton<LaneManager> {

    public int CurrentLane { get { return currentLane; } set { currentLane = value; } }
    public int TargetLane { get { return targetLane; } set { targetLane = value; } }

    public float laneWidth = 2f;
    public int numberOfLanes = 5;
    public float timeToSwitchLane = 0.15f;
    int currentLane = 2;
    int targetLane = 2;
    int currentLaneSet = 0;

    Dictionary<int, Lane> laneDic = new Dictionary<int, Lane>();

    public class Lane
    {
        public GameObject gObject;
        public MaterialSwapper swapper;
        public int index;
        public bool triggered = false;

        public Lane(GameObject g, int i)
        {
            gObject = g;
            swapper = g.GetComponent<MaterialSwapper>();
            index = i;
        }
    }

    protected override void Awake()
    {
        base.Awake();
        GameManager.Instance.onRestart += Reset;

        //initLaneDictionary
        int i = 0;
        foreach (Transform t in transform)
        {
            laneDic.Add(i, new Lane(t.gameObject, i));
            ++i;
        }
    }

    private void Reset()
    {
         currentLane = 2;
         targetLane = 2;
         currentLaneSet = 0;

         TurnOffAllTheLanes();
    }

    public void TurnOffAllTheLanes()
    {
        foreach (Lane lane in laneDic.Values)
        {
            lane.triggered = false;
            lane.swapper.Reset();
        }
    }

    public int GetRandomAdjacentLane()
    {
        int index= 0;
        float seed = Random.Range(0f,1f);
        if (seed < 0.5f)
            index = CurrentLane - 1;
        else
            index = CurrentLane + 1;

        if (index < 0) index = numberOfLanes - 1;
        else if (index > numberOfLanes - 1) index = 0;

        return index;
    }

    public void ActivateAllLanes()
    {
        foreach (Lane lane in laneDic.Values)
        {
            lane.swapper.Reset();
            lane.triggered = true;
            lane.swapper.SwapMaterial();
        }
        
    }

    public void ActivateLanes()
    {
        if (currentLaneSet == 0)
        {
            foreach (Lane lane in laneDic.Values)
            {
                if (lane.index % 2 == 0)
                {
                    lane.triggered = true;
                    lane.swapper.SwapMaterial();
                }
                else
                {
                    lane.swapper.Reset();
                    lane.triggered = false;
                }
            }
        }
        else
        {
            foreach (Lane lane in laneDic.Values)
            {
                if (lane.index % 2 != 0)
                {
                    lane.triggered = true;
                    lane.swapper.SwapMaterial();
                }
                else
                {
                    lane.swapper.Reset();
                    lane.triggered = false;
                }
            }
        }
        ++currentLaneSet;
        currentLaneSet = currentLaneSet % 2;
    }


    public void ActivateLane(int index)
    {
        foreach (Lane lane in laneDic.Values)
        {
                lane.triggered = false;
                lane.swapper.Reset();
        }
        laneDic[index].triggered = true;
        laneDic[index].swapper.SwapMaterial();

    }

    public float GetLaneX(int lane)
    {
        return lane * laneWidth;
    }


    internal bool IsPlayerOnTriggeredLane()
    {
        return laneDic[currentLane].triggered;
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyManager : MonoSingleton<EnemyManager> {

    public class EnemyContainer
    {
        public Enemy enemy;
        public GameObject g;
        public Rigidbody rb;
        public Transform t;

        public EnemyContainer(Enemy e, GameObject go, Rigidbody r, Transform tr)
        {
            enemy = e;
            g = go;
            rb = r;
            t = tr;
        }
    }

    public float speed = 0.1f;
    public float frecquency = 2f;

    List<EnemyContainer> pool = new List<EnemyContainer>();
    List<EnemyContainer> activeEnemies = new List<EnemyContainer>();

    Transform t = null;
    protected override void Awake()
    {
        base.Awake();
        t = GetComponent<Transform>();
        for (int i = 0; i < 10; ++i)
        {
            pool.Add(new EnemyContainer(t.GetChild(i).GetComponent<Enemy>(), t.GetChild(i).gameObject, t.GetChild(i).GetComponent<Rigidbody>(), t.GetChild(i).GetComponent<Transform>()));
        }
        InvokeRepeating("ActivateEnemy", 1f, frecquency);
    }

    void ActivateEnemy()
    {
        if (pool.Count > 0)
        {
            //take an enemy from the pool, set it "active" 
            EnemyContainer e = pool[0];

            e.t.position = new Vector3(LaneManager.instance.GetLaneX(Random.Range(0, LaneManager.instance.numberOfLanes - 1)), t.position.y, t.position.z);
            e.enemy.Activate();
            pool.Remove(e);
            activeEnemies.Add(e);
        }
    }

    void FixedUpdate()
    {
        for (int i = 0; i < activeEnemies.Count; ++i)
        {
               activeEnemies[i].rb.MovePosition(new Vector3 (activeEnemies[i].t.position.x, activeEnemies[i].t.position.y, activeEnemies[i].t.position.z - speed));
        }
    }

    void OnTriggerEnter(Collider other)
    {
        EnemyContainer container = null;
        for (int i = 0; i < activeEnemies.Count; ++i)
        {
            if (other.gameObject == activeEnemies[i].g)
            {
                container = activeEnemies[i];
                break;
            }
        }

        container.enemy.Deactivate();
        activeEnemies.Remove(container);
        pool.Add(container);
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WorldMover : MonoBehaviour {
    public float tileSize = 20f;
    public float speedPerFrame = 1f;
    public Transform poolFolder = null;
    List<GameObject> pool = new List<GameObject>();
    Queue<GameObject> active = new Queue<GameObject>();
    Transform t = null;
    float movement = 0f;

	// Use this for initialization
	void Awake () {
	    //populate list with childs
        GetActiveTiles();
        GetPoolTiles();
	}

    void GetActiveTiles()
    {
        t = GetComponent<Transform>();
        for (int i = 0; i < t.childCount; ++i)
        {
            active.Enqueue(t.GetChild(i).gameObject);
        }
    }

    void GetPoolTiles()
    {
        for (int i = 0; i < poolFolder.childCount; ++i)
        {
            pool.Add(poolFolder.GetChild(i).gameObject);
        }
    }
	
	// Update is called once per frame
	void Update () {
        if (!GameManager.Instance.IsPlaying()) return;
	//move active ones
        movement += speedPerFrame;

        // if moved of the size of a tile ..move end one in the available pool , and a random in pool to the start

        if (movement > tileSize)
        {
            movement = movement - tileSize; 
            GameObject headTile = active.Dequeue();
            headTile.SetActive(false);
            pool.Add(headTile);
            GameObject randomTile = pool[Random.Range(0, pool.Count)];
            pool.Remove(randomTile);
            active.Enqueue(randomTile);
            randomTile.SetActive(true);
            Transform t = randomTile.transform;
            randomTile.transform.position = new Vector3(t.position.x, t.position.y, tileSize * (active.Count -1) - movement);
        }

        foreach (GameObject tile in active)
        {
            tile.transform.position += speedPerFrame * new Vector3(0f, 0f, -1f);
        }
        
	}
}

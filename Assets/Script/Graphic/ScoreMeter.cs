﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoreMeter : MonoBehaviour {

    public float speed = 1f;
    public List<Transform> parts = new List<Transform>();
    int[] targetValues = new int[3];
    float[] currentValues = new float[3];

    public void SetTargetValue(int value)
    {
        targetValues[0] = value.ToString()[0];
        targetValues[1] = value.ToString()[1];
        targetValues[2] = value.ToString()[2];
    }

    void DisplayValue(float[] valueArray)
    {
        int i =0;
        foreach( Transform part in parts)
        {
            part.localEulerAngles = new Vector3(part.localEulerAngles.x,
              part.localEulerAngles.y,
              valueArray[i]
              );
        }
    }

	void UpdateValues()
    {

    }

	// Update is called once per frame
	void Update () {
	    //
        UpdateValues();
	}
}

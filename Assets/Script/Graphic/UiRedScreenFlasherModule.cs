﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiRedScreenFlasherModule : UiSingleton<UiRedScreenFlasherModule> {

    public float seconds = 0.15f;
    float timer = 0f;
    Image gradient = null;

    protected override void Awake()
    {
        base.Awake();
        gradient = GetComponent<Image>();
        GameManager.Instance.onRestart += Reset;
    }

    private void Reset()
    {
        timer = 0f;
        Color oldColor = gradient.color;
        Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, 0f);
        gradient.color = newColor; 
    }

	public void Flash()
    {
        timer = seconds;
    }

    void Update()
    {
        if(timer > 0f)
        {
            timer -= Time.deltaTime;
            float alpha;
            if (timer <= 0f)
            {
                alpha = 0f;
                timer = 0f;
            }
            else
            {
                alpha = 1f - timer / seconds;
            }
            Color oldColor = gradient.color;
            Color newColor = new Color(oldColor.r, oldColor.g, oldColor.b, alpha);
            gradient.color = newColor; 
        }
    }
}

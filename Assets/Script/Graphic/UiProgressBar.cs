﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UiProgressBar : UiSingleton<UiProgressBar> {

    Image progressBar = null;

    protected override void Awake()
    {
        base.Awake();
        progressBar = GetComponent<Image>();
        GameManager.Instance.onRestart += Reset;
    }

    void Reset()
    {
        progressBar.fillAmount = 0f;
    }

    public void SetProgressBar(float p)
    {
        progressBar.fillAmount = p;
    }
}

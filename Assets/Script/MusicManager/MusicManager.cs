﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MusicManager : MonoSingleton<MusicManager> {

    List<Track> tracks = new List<Track>();
    private bool isGlobalSequenceCompleted = false;
    private int currentIndex = 0;
    float currentBeat = 0f;
    float timer = 0f;
    int currentTrackIndex = 1; 

    Track CurrentTrack { get { return tracks[currentTrackIndex]; } }

	void Start () {

        GameManager.Instance.onPaused += Paused;
        GameManager.Instance.onUnpaused += UnPaused;
        GameManager.Instance.onRestart += Restart;

        CreateList();
        PopulateLists();
	}

    private void CreateList()
    {
        foreach (Transform t in transform)
        {
            tracks.Add(t.GetComponent<Track>());
        }
    }

    private void Restart()
    {
        CurrentTrack.completeAudioSource.time = 0f;
        CurrentTrack.completeAudioSource.Play();
        isGlobalSequenceCompleted = false;
        currentIndex = 0;
    }

    private void UnPaused()
    {
        CurrentTrack.completeAudioSource.UnPause();
    }

    private void Paused()
    {
        CurrentTrack.completeAudioSource.Pause();
    }

    private void PopulateLists()
    {
        //foreach (Track t in tracks)
        //{
        //    t.beats.Clear();
        //    for (float i = 4.5f; i < 119.5f - 4.5f; ++i)
        //    {
        //        t.beats.Add(i - AudioVisualizer.Instance.flyTime);
        //    }
        //}
    }

    public void CollectedAGem()
    {
        ++CurrentTrack.currentGems;
    }

    public void SetTrack(int i)
    {
        currentTrackIndex = i;
    }
	
	// Update is called once per frame
	void Update () {
        
        UpdateTimer();
        UpdateExecuteNextGlobalPhrase();
	}

    private void UpdateTimer()
    {
        timer = CurrentTrack.completeAudioSource.time;
        currentBeat = (CurrentTrack.bpm / 60f) * timer;
        UiProgressBar.Instance.SetProgressBar((float)currentBeat / CurrentTrack.totalNumberOfBeats);
        if (currentBeat >= CurrentTrack.totalNumberOfBeats)
        {
            GameManager.Instance.StartGame();
            GameManager.Instance.PauseTheGame();
            UiManager.instance.PushUiScreen(UiTapToStart.Instance);
        }
    }

    private void UpdateExecuteNextGlobalPhrase()
    {
        if (CurrentTrack.beats.Count == 0 || isGlobalSequenceCompleted)
            return;

        if (currentBeat >= CurrentTrack.beats[currentIndex])
        {
            AudioVisualizer.Instance.ActivateMusicGem();
            ++currentIndex;
            if (currentIndex > CurrentTrack.beats.Count - 1)
            {
                isGlobalSequenceCompleted = true;
            }
        }
    }

    public int GetMaxNumberOfGems()
    {
        return CurrentTrack.beats.Count;
    }
}

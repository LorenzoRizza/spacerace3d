﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AudioVisualizer : MonoSingleton<AudioVisualizer> {

    public float flyTime = 4f;

    public class MusicGemData
    {
        public MusicGem brain;
        public GameObject gemGameObject;
        public Rigidbody rb;
        public Transform t;
        public int lane;

        public MusicGemData(MusicGem b, GameObject go, Rigidbody r, Transform tr, int l = 0)
          {
              brain = b;
              gemGameObject = go;
              rb = r;
              t = tr;
              lane = l;
         }
    }

    Transform t = null;
    List<MusicGemData> pool = new List<MusicGemData>();
    List<MusicGemData> activeGems = new List<MusicGemData>();
    float Speed { get { return (t.position.z / flyTime) * 2f * Time.fixedDeltaTime; } }
    int lastLaneAssignedToGem = 2;
    int gemCounter = 0;

    protected override void Awake()
    {
        GameManager.Instance.onRestart += Reset;
        base.Awake();
        t = GetComponent<Transform>();
        for (int i = 0; i < 10; ++i)
        {
            pool.Add(new MusicGemData(
                t.GetChild(i).GetComponent<MusicGem>(),
                t.GetChild(i).gameObject,
                t.GetChild(i).GetComponent<Rigidbody>(),
                t.GetChild(i).GetComponent<Transform>()
                ));
        }
    }

    private void Reset()
    {
        gemCounter = MusicManager.instance.GetMaxNumberOfGems();

        for (int i = 0; i < activeGems.Count; ++i)
        {
            activeGems[i].brain.Deactivate();
            pool.Add(activeGems[i]);
        }
        activeGems.Clear();
    }

    void FixedUpdate()
    {
        for (int i = 0; i < activeGems.Count; ++i)
        {
            activeGems[i].rb.MovePosition(
                new Vector3(
                activeGems[i].t.position.x,
                activeGems[i].t.position.y,
                activeGems[i].t.position.z - Speed
                )
                );
        }
    }

    public void ActivateMusicGem()
    {
        if (pool.Count > 0)
        {
            MusicGemData e = pool[0];
            float random = Random.Range(0f, 1f);


            e.lane = random > 0.5f?  ++lastLaneAssignedToGem : --lastLaneAssignedToGem;

            if (e.lane < 0) e.lane = 1;
            else if (e.lane > LaneManager.instance.numberOfLanes - 1) e.lane = LaneManager.Instance.numberOfLanes - 2;

            lastLaneAssignedToGem = e.lane;

            e.t.position = new Vector3(LaneManager.Instance.GetLaneX(e.lane), t.position.y, t.position.z);
            e.brain.Activate();
            pool.Remove(e);
            activeGems.Add(e);
        }
    }

    public void DeactivateMusicGem(MusicGem gem, bool activateLane)
    {
        MusicGemData data = null;
        for (int i = 0; i < activeGems.Count; ++i)
        {
            if (gem == activeGems[i].brain)
            {
                data = activeGems[i];
                break;
            }
        }

        --gemCounter;
        data.brain.Deactivate();
        activeGems.Remove(data);
        pool.Add(data);

        if(activateLane)
        {
            LaneManager.Instance.ActivateLane(data.lane);
        }
        else
        {
            ScoreModule.Instance.GetDamage();
            UiRedScreenFlasherModule.Instance.Flash();
            LaneManager.Instance.TurnOffAllTheLanes();
        }

        if (gemCounter ==0)
        {
            LaneManager.Instance.ActivateAllLanes();
        }
    }
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Track : MonoBehaviour {
    public float bpm = 180f;
    public float totalNumberOfBeats = 120f;
    public List<float> beats = new List<float>();
    public AudioSource completeAudioSource = null;
    public float bestProgress = 0f;
    public int currentGems = 0;
    public float CurrentProgress { get { return (float)currentGems / beats.Count; } }

    void Awake()
    {
        //retreive best score
        GameManager.Instance.onRestart += Reset;
    }

    private void Reset()
    {
        currentGems = 0;
    }
}

﻿using UnityEngine;
using System.Collections;

public class MusicGem : MonoBehaviour {

    Renderer myRenderer = null;
    Collider myCollider = null;

    void Awake()
    {
        myCollider = GetComponent<Collider>();
        myRenderer = GetComponent<Renderer>();
        Deactivate();
    }

    public void Activate()
    {
        myRenderer.enabled = true;
        myCollider.enabled = true;
    }

    public void Deactivate()
    {
        myRenderer.enabled = false;
        myCollider.enabled = false;
    }

    void OnTriggerEnter(Collider other)
    {
        bool activateLane = false;

        if(other.gameObject.layer == Layers.kPlayer)
        {
            activateLane = true;
            MusicManager.Instance.CollectedAGem();
        }

        AudioVisualizer.Instance.DeactivateMusicGem(this, activateLane);
    }
}
